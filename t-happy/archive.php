<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="content">
<div class="pcbgh2"><h2>新着情報</h2></div>
<ul class="link">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post();
	$img_id = get_post_meta($post->ID, 'thumbnail_img', true);
	$img = wp_get_attachment_image_src($img_id, 'full');
	


	
	echo '<li><dl class="cf">';
	echo '<dt>'.get_the_time("Y.m.d").'</dt>';
	echo '<dd';
	if(!empty($img)){	echo ' class="on_thumb';};
	echo '"><a href="'.get_the_permalink().'">'.get_the_title().'</a></dd>';
	if(!empty($img)){
		echo '<dd class="thumb"><a href="'.get_the_permalink().'"><img src="'.$img[0].'"></a></dd>';};
	echo '</dl></li>';
?>
	
<?php endwhile; endif;?>
</ul>
<div class="pager cf">
	<?php if (function_exists("pagination")) { pagination($additional_loop->max_num_pages); } ?>
</div>

</div>
<?php get_footer(); ?>
