<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="content">
<div class="slide_bg">
	<ul class="bxslider cf">
		<li><a href="<?php bloginfo('url'); ?>/episode/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slide01.png" width="100%" alt=""></a></li>
<!--		<li><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slide02.png" width="100%" alt=""></a></li>
		<li><a href="<?php bloginfo('url'); ?>/form/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slide03.png" width="100%" alt=""></a></li>-->
		<li><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slide05.png" width="100%" alt=""></a></li>
		<li><a href="<?php bloginfo('url'); ?>/about/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/slide04.png" width="100%" alt=""></a></li>

	</ul>
</div>
<div class="cm1">
	<p class="bg_gra pcnone">本キャンペーンは、「結婚部門」「妊娠部門」「出産部門」「子育て部門」の4部門で、<br>県民の皆様から「幸せエピソード」を、写真や映像とともに募集し、16組（各部門4組）を「TOYAMAハッピーライフCM」として紹介、放送するものです。</p>
	<img src="<?php bloginfo('template_url'); ?>/common/images/top/img_cm_sp.png" width="100%" class="imgSwitch" alt="">
	<p></p>
	<p class="bg_gra spnone">本キャンペーンは、「結婚部門」「妊娠部門」「出産部門」「子育て部門」の4部門で、<br>県民の皆様から「幸せエピソード」を、写真や映像とともに募集し、16組（各部門4組）を「TOYAMAハッピーライフCM」として紹介、放送するものです。</p>
</div>
<div class="cm3">
	<div class="cm_wrap">
		<h3><span class="spnone"><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_episode.png" alt="富山の皆さんの「幸せエピソード」をご紹介します！"></span><span class="pcnone">富山の皆さんの「幸せエピソード」をご紹介します！</span></h3>
<script>
$(window).on('load resize', function() {
		var w = $(window).width();

		if ( w <= 769 ) {
		$('.cm3 li dl').css("height",'auto');
		} else {
    var columns = 4;
    var liLen = $('.cm3 li dl').length;
    var lineLen = Math.ceil(liLen / columns);
    var liHiArr = [];
    var lineHiArr = [];

    for (i = 0; i <= liLen; i++) {
        liHiArr[i] = $('.cm3 li dl').eq(i).height();
        if (lineHiArr.length <= Math.floor(i / columns)
            || lineHiArr[Math.floor(i / columns)] < liHiArr[i])
        {
            lineHiArr[Math.floor(i / columns)] = liHiArr[i];
        }
    }
    $('.cm3 li dl').each(function(i){
        $(this).css('height',lineHiArr[Math.floor(i / columns)] + 'px');
    });
		}
});
</script>

		<ul class="cf">
		<?php
		$cat_list = array('bridal','pregnancy','birth','child');
		foreach ($cat_list as $cat_now){
			query_posts(
				array(
				'post_type' => 'episode',
				'posts_per_page' => 1,
				'tax_query' => array(
						array(
								'taxonomy' => 'episode_cat',
								'field' => 'slug',
								'terms' => $cat_now,
								),
						),
				 )
			);
			if (have_posts()) : while (have_posts()) : the_post();
				$image_id = SCF::get('img_thumb');
				$image = wp_get_attachment_image_src($image_id, 'full');
				$txt_name = get_post_meta($post->ID, 'txt_name', true);
				$txt_episode = nl2br(get_post_meta($post->ID, 'txt_episode', true));
				$url_youtube = get_post_meta($post->ID, 'url_youtube', true);
				$get_term = get_term_by("slug", $cat_now, 'episode_cat');

				echo '<li class="list_'.$cat_now.'">';
				echo '<h4><span class="spnone"><img src="'.get_bloginfo('template_url').'/common/images/top/ttl_episode_'.$cat_now.'.png" alt=""></span><span class="pcnone">'.$get_term->name.'</span></h4>';
				echo '<dl>';
				echo '<dt><a href="https://www.youtube.com/embed/'.$url_youtube.'?rel=0" class="colorbox_youtube" style="background-image:url('.$image[0].')"><img src="'.get_bloginfo('template_url').'/common/images/episode/btn_start.png" width="100%"></a></dt>';
				echo '<dd>';
				if (!empty($txt_name)) { echo '<p class="tltle_name">'.$txt_name.'</p>';}
				echo '<h5>'.get_the_title().'</h5>';
				if (!empty($txt_episode)) { echo '<p>'.$txt_episode.'</p>';}
				echo '</dd>';
				echo '</dl>';
				echo '</li>';

			endwhile; endif; wp_reset_query();
		}
		?>
		</ul>
		<p class="btn_area"><a href="<?php bloginfo('url'); ?>/episode/"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_more_pc.png" class="spnone" alt="エピソードをもっと見る"><img src="<?php bloginfo('template_url'); ?>/common/images/top/btn_more_sp.png" width="100%" class="pcnone" alt="エピソードをもっと見る"></a></p>
	</div>
</div>
<div id="infomation" class="info cf">
	<div class="pc_left">
		<h3><img src="<?php bloginfo('template_url'); ?>/common/images/top/ttl_info.png" width="100%" alt="">新着情報<span>information</span></h3>
		<ul>
			<?php
			query_posts(
				array(
				'post_type' => 'information',
				'posts_per_page' => 5
				 )
			);
			if(have_posts()): while(have_posts()): the_post(); ?>
			<li>
				<dl class="cf">
				<dt><?php the_time('Y.m.d'); ?></dt>
				<dd><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></dd>
				</dl>
			</li>
			<?php endwhile; endif; wp_reset_query(); ?>
		</ul>
		<a href="<?php bloginfo('url'); ?>/information/" class="info_list">一覧を見る</a>
	</div>
	<div class="pc_right">
		<div class="fb_area">
			<div class="fb-page" data-href="https://www.facebook.com/Toyama%E3%83%8F%E3%83%83%E3%83%94%E3%83%BC%E3%83%A9%E3%82%A4%E3%83%95%E3%82%AD%E3%83%A3%E3%83%B3%E3%83%9A%E3%83%BC%E3%83%B32015-1511270559172803/?fref=ts" data-width="375" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true"></div>
		</div>
		<div class="btn_tw"><a href="https://twitter.com/TOYAMAhappylife" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/top/icon_tw.png" width="100%" alt="">TOYAMA HAPPY LIFE のつぶやきをみる</a></div>
	</div>
</div>
</div>

<?php get_footer(); ?>
