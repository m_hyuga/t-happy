<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="content" class="junbi">
		<div class="pcbgh2"><h2>&emsp;</h2></div>
		<p style="text-align:center; margin:4em 0 8em;">申し訳ありません。<br>お探しのページは見つかりませんでした。<br><br>
		<a href="<?php bloginfo('url'); ?>/">トップページへ戻る</a></p>
	</div>

<?php get_footer(); ?>
