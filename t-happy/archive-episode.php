<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="content">
<div class="pcbgh2"><h2>幸せエピソードを見る</h2></div>
		<p class="ico_img spnone"><img src="<?php bloginfo('template_url'); ?>/common/images/episode/img_episode.png"  alt=""></p>
		<ul class="list_btn cf">
			<li class="btn_bridal"><a href="#area_bridal">結婚部門</a></li>
			<li class="btn_pregnancy"><a href="#area_pregnancy">妊娠部門</a></li>
			<li class="btn_birth"><a href="#area_birth">出産部門</a></li>
			<li class="btn_child"><a href="#area_child">子育て部門</a></li>
		</ul>
		<?php
		$cat_list = array('bridal','pregnancy','birth','child');
		foreach ($cat_list as $cat_now){
			query_posts(
				array(
				'post_type' => 'episode',
				'posts_per_page' => -1,
				'tax_query' => array(
						array(
								'taxonomy' => 'episode_cat',
								'field' => 'slug',
								'terms' => $cat_now,
								),
						),
				 ) 
			);
			if (have_posts()) : while (have_posts()) : the_post();
				$image_id = SCF::get('img_thumb');
				$image = wp_get_attachment_image_src($image_id, 'full');
				$txt_name = get_post_meta($post->ID, 'txt_name', true);
				$txt_episode = nl2br(get_post_meta($post->ID, 'txt_episode', true));
				$url_youtube = get_post_meta($post->ID, 'url_youtube', true);
				$get_term = get_term_by("slug", $cat_now, 'episode_cat');
				if(isFirst()){
					echo '<h4 id="area_'.$cat_now.'"><span class="h4_title"><span class="spnone"><img src="'.get_bloginfo('template_url').'/common/images/episode/img_'.$cat_now.'.png" alt=""></span>'.$get_term->name.'</span></h4>';
					echo '<div class="list_wrap"><ul class="list_movie cf">';
				};
				
				echo '<li>';
				echo '<dl class="cf">';
				echo '<dt><a href="https://www.youtube.com/embed/'.$url_youtube.'?rel=0" class="colorbox_youtube" style="background-image:url('.$image[0].')"><img src="'.get_bloginfo('template_url').'/common/images/episode/btn_start.png" width="100%"></a></dt>';
				echo '<dd>';
				echo '<div class="title_list cf';
				if (!empty($txt_name)) { echo ' name_on';}
				echo '">';
				if (!empty($txt_name)) { echo '<p>'.$txt_name.'</p>';}
				echo '<h5>'.get_the_title().'</h5>';
				echo '</div>';
				if (!empty($txt_episode)) { echo '<p>'.$txt_episode.'</p>';}
				echo '</dd>';
				echo '</dl>';
				echo '</li>';
				
				if(isLast()){ echo '</ul></div>';};
			
			endwhile; endif; wp_reset_query();
		}
		?>

</div>
<?php get_footer(); ?>
