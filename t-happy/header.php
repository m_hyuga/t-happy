<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html lang="ja-JP">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
	<meta name="format-detection" content="telephone=no" />
	<meta name="description" content="あなたの幸せエピソードがテレビCMになります。
	本キャンペーンは、「結婚部門」「妊娠部門」「出産部門」「子育て部門」の4部門で、県民の皆様から「幸せエピソード」を、写真や映像とともに募集し、16組（各部門4組）を「TOYAMAハッピーライフCM」として紹介、放送するものです。（とやまハッピーライフ｜富山ハッピーライフ）">
	<meta name="keyword" content="富山県,結婚,妊娠,出産,子育て,富山ハッピーライフ,富山 ハッピーライフ,とやまハッピーライフ,とやま ハッピーライフ,TOYAMAハッピーライフ,TOYAMA ハッピーライフ">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/reset.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/common.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/editor-style.css" />
	<?php if (is_home()): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/top.css" />
	<?php endif; if (is_page('about')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/about.css" />
	<?php endif; if (is_page('policy')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/policy.css" />
	<?php endif; if (is_page('download') || is_page('link')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/link.css" />
	<?php endif; if (is_page('form')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/form.css" />
	<?php endif; if (is_page('kosodate')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/kosodate.css" />
        <?php endif; if (is_page('sitemap')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/sitemap.css" />
	<?php endif; if (is_post_type_archive('information') || is_singular('information')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/information.css" />
	<?php endif; if (is_post_type_archive('episode')): ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/css/episode.css" />
	<?php endif; ?>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/common.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.smoothScroll.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/jquery.bxslider/jquery.bxslider.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/common/js/colorbox/colorbox.css" />
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/common/js/colorbox/jquery.colorbox-min.js"></script>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
	<script>
		$(document).ready(function(){
			$('.bxslider').bxSlider({
				pager:false,
				auto:true
			});
		});
	</script>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-70981252-1', 'auto');
	ga('send', 'pageview');

	</script>
</head>

<body <?php body_class(); ?>>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="wrap">

<header>
	<div class="head_inner">
		<h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/common/images/common/mainlogo_sp.png" class="imgSwitch" width="100%" alt=""></a></h1>
		<div id="spMenu_btn"><span></span></div>
		<div class="pcMenu_area">
			<div class="fntSize">文字サイズ<span class="changeBtn">小</span><span class="changeBtn">中</span><span class="changeBtn">大</span></div>
			<ul class="subnav cf">
				<li><a href="<?php bloginfo('url'); ?>/download/">ダウンロード</a></li>
				<li><a href="<?php bloginfo('url'); ?>/link/">関連リンク</a></li>
				<li><a href="<?php bloginfo('url'); ?>/policy/">サイトポリシー</a></li>
				<li><a href="<?php bloginfo('url'); ?>/sitemap/">サイトマップ</a></li>
			</ul>
		</div>
	</div>
	<nav>
		<div class="nav_inner">
			<ul class="menu cf">
				<li class="btn_menu01"><a href="<?php bloginfo('url'); ?>/information/">新着情報</a></li>
				<li class="btn_menu02"><a href="<?php bloginfo('url'); ?>/about/">TOYAMA<br>ハッピーライフとは？</a></li>
				<li class="btn_menu03"><a href="<?php bloginfo('url'); ?>/junbi/">ハッピー<br>ライフステージ</a></li>
				<li class="btn_menu05"><a href="<?php bloginfo('url'); ?>/episode/">幸せエピソード<br>を見る</a></li>
				<li class="btn_menu06"><a href="<?php bloginfo('url'); ?>/kosodate/">とやま子育て<br>応援団</a></li>
			</ul>
		</div>
	</nav>
</header>

