$(function(){

	//実行
	$(window).on('load resize', function(){ //ロード時とリサイズ時に実行
		windowWidth = $(window).width(); //windowサイズ
		breakPoint = 769 //ブレイクポイント
		footerscroll();
		if(windowWidth <= breakPoint) {
			//SP時の処理
			spMenu_btn();
			spmenu();
		} else {
			//PC時の処理
			pcmenu();
			fontsize_button();
		}
		//共通
		imgSwitch();
		pageTop();
	});
	spMenu_toggle();

function spMenu_btn() {
	//スマホのメニューボタン関連
	var btnWidth = $('#spMenu_btn').width();
	var offsetTop = $('#spMenu_btn').offset().top;
	$('#spMenu_btn').height(btnWidth);
	$('.menu').css({'top':offsetTop+btnWidth});
}
function spMenu_toggle() {
	$('#spMenu_btn').on('click', function(){
		$('.menu').stop(true,false).fadeToggle();
	});
}
function spmenu() {
	$('.menu').css({'display':'none'});
}
function pcmenu() {
	$('.menu').css({'display':'block'});
}


function imgSwitch() {
	//画面幅によって画像の切り替え
	var $setElem = $('.imgSwitch'),
	pcName = '_pc',
	spName = '_sp',
	replaceWidth = 769;

	$setElem.each(function(){
		var $this = $(this);
		function imgSize(){
			var windowWidth = parseInt($(window).width());
			if(windowWidth >= replaceWidth) {
				$this.attr('src',$this.attr('src').replace(spName,pcName)).css({visibility:'visible'});
			} else if(windowWidth < replaceWidth) {
				$this.attr('src',$this.attr('src').replace(pcName,spName)).css({visibility:'visible'});
			}
		}
		$(window).resize(function(){imgSize();});
		imgSize();
	});
}

function pageTop() {
	$('#pagetop').on('click', function(){
		$('html,body').animate({scrollTop:0}, 500);
		return false;
	});
}
$(".colorbox").colorbox({maxWidth:'95%',maxHeight:'95%'});
$(".colorbox_youtube").colorbox({
iframe:true,
innerWidth:650,
innerHeight:460,
maxWidth:'95%',
maxHeight:'95%'
});

$("#pagetop").hide();
 // ↑ページトップボタンを非表示にする

function footerscroll() {
$(window).on("scroll", function() {

	if ($(this).scrollTop() > 150) {
			// ↑ スクロール位置が100よりも小さい場合に以下の処理をする
			$('#pagetop').slideDown("fast");
			// ↑ (100より小さい時は)ページトップボタンをスライドダウン
	} else {
			$('#pagetop').slideUp("fast");
			// ↑ それ以外の場合の場合はスライドアップする。
	}
	 
// フッター固定する

	scrollHeight = $(document).height(); 
	// ドキュメントの高さ
	scrollPosition = $(window).height() + $(window).scrollTop(); 
	//　ウィンドウの高さ+スクロールした高さ→　現在のトップからの位置
	footHeight = $("#footer").innerHeight();
	// フッターの高さ
					 
	if ( scrollHeight - scrollPosition  <= footHeight ) {
	// 現在の下から位置が、フッターの高さの位置にはいったら
	//  ".page-top"のpositionをabsoluteに変更し、フッターの高さの位置にする        
			$("#pagetop").css({
					"position":"absolute",
					"bottom": footHeight+20
			});
	} else {
	// それ以外の場合は元のcssスタイルを指定
			$("#pagetop").css({
					"position":"fixed",
					"bottom": 20
			});
	}
});
}






function fontsize_button(){

	$(function(){
		fontsizeChange();
	});

	function fontsizeChange(){

		var changeArea = $("#content");			//フォントサイズ変更エリア
		var btnArea = $(".fntSize");				//フォントサイズ変更ボタンエリア
		var changeBtn = btnArea.find(".changeBtn");	//フォントサイズ変更ボタン
		var fontSize = [90,100,120];				//フォントサイズ（HTMLと同じ並び順、幾つでもOK、単位は％）
		var ovStr = "";							//ロールオーバー画像ファイル末尾追加文字列（ロールオーバー画像を使用しない場合は値を空にする）
		var activeClass = "active";					//フォントサイズ変更ボタンのアクティブ時のクラス名
		var defaultSize = 1;						//初期フォントサイズ設定（HTMLと同じ並び順で0から数値を設定）
		var cookieExpires = 7;						//クッキー保存期間
		var sizeLen = fontSize.length;
		var useImg = ovStr!="" && changeBtn.is("[src]");

		//現在クッキー確認関数
		function nowCookie(){
			return $.cookie("fontsize");
		}

		//画像切替関数
		function imgChange(elm1,elm2,str1,str2){
			elm1.attr("src",elm2.attr("src").replace(new RegExp("^(\.+)"+str1+"(\\.[a-z]+)$"),"$1"+str2+"$2"));
		}

		//マウスアウト関数
		function mouseOut(){
			for(var i=0; i<sizeLen; i++){
				if(nowCookie()!=fontSize[i]){
					imgChange(changeBtn.eq(i),changeBtn.eq(i),ovStr,"");
				}
			}
		}

		//フォントサイズ設定関数
		function sizeChange(){
			changeArea.css({fontSize:nowCookie()+"%"});
		}

		//クッキー設定関数
		function cookieSet(index){
			$.cookie("fontsize",fontSize[index],{path:'/',expires:cookieExpires});
		}

		//初期表示
		if(nowCookie()){
			for(var i=0; i<sizeLen; i++){
				if(nowCookie()==fontSize[i]){
					sizeChange();
					var elm = changeBtn.eq(i);
					if(useImg){
						imgChange(elm,elm,"",ovStr);
					}
					elm.addClass(activeClass);
					break;
				}
			}
		}
		else {
			cookieSet(defaultSize);
			sizeChange();
			var elm = changeBtn.eq(defaultSize);
			if(useImg){
				imgChange(elm,elm,"",ovStr);
				imgChange($("<img>"),elm,"",ovStr);
			}
			elm.addClass(activeClass);
		}

		//ホバーイベント（画像タイプ）
		if(useImg){
			changeBtn.each(function(i){
				var self = $(this);
				self.hover(
				function(){
					if(nowCookie()!=fontSize[i]){
						imgChange(self,self,"",ovStr);
					}
				},
				function(){
					mouseOut();
				});
			});
		}

		//クリックイベント
		changeBtn.click(function(){
			var index = changeBtn.index(this);
			var self = $(this);
			cookieSet(index);
			sizeChange();
			if(useImg){
				mouseOut();
			}
			if(!self.hasClass(activeClass)){
				changeBtn.not(this).removeClass(activeClass);
				self.addClass(activeClass);
			}
		});

	}

};

});