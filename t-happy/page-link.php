<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="content">
<div class="pcbgh2"><h2>関連リンク</h2></div>
<ul class="link">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php endwhile; endif;
	$repeat_group = scf::get('link_field');
	foreach ( $repeat_group as $field_name => $field_value ) :
		$link_title = $field_value['link_title'];
		$link_url = $field_value['link_url'];
		echo '<li>';
		if (!empty($link_url)) { echo '<a href="'.$link_url.'" target="_blank">';}
		echo $link_title;
		if (!empty($link_url)) { echo '</a>';}
		echo '</li>';
	 endforeach;?>
</ul>
</div>

<?php get_footer(); ?>
