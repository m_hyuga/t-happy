<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
	<div id="pagetop"><img src="<?php bloginfo('template_url'); ?>/common/images/common/pagetop.png" width="100%" alt=""></div>
<div id="footer">
<footer>
	<h3>関連サイト</h3>
	<ul class="relation_site cf">
		<li><a href="http://www.pref.toyama.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_bnr01_sp.jpg" class="imgSwitch" width="100%" alt=""></a></li>
		<li><a href="http://kosodate.derideri.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_bnr02_sp.jpg" class="imgSwitch" width="100%" alt=""></a></li>
		<li><a href="http://www.pref.toyama.jp/sections/3009/hp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_bnr03_sp.jpg" class="imgSwitch" width="100%" alt=""></a></li>
		<li><a href="http://toyama-mienet.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_bnr04_sp.jpg" class="imgSwitch" width="100%" alt=""></a></li>
	</ul>
	<div class="foot_inner">
		<div class="cf">
			<h2><img src="<?php bloginfo('template_url'); ?>/common/images/common/foot_logo.jpg" width="100%" alt=""></h2>
			<div class="foot_deta">
				<p>&copy; 2015 富山県   |    お問い合わせ先 「TOYAMA ハッピーライフキャンペーン2015」<span>事務局 TEL:０７６－４３１－８５１４</span>&lt;(株)大広北陸内&gt; 平日９：３０～１７：３０（土日祝休 12月29日～1月5日を除く）</p>
				<p>※本事業は富山県からの業務委託を受け、<span>株式会社 大広北陸が実施・運営するものです。</span></p>
			</div>
		</div>
	</div>
</footer>
</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
