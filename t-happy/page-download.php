<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="content">
<div class="pcbgh2"><h2>ダウンロード</h2></div>
<ul class="link">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php endwhile; endif;
	$repeat_group = scf::get('download_field');
	foreach ( $repeat_group as $field_name => $field_value ) :
		$file_title = $field_value['file_title'];
		$file_id = $field_value['file_item'];
		$file = wp_get_attachment_url($file_id);
		$img_id = $field_value['img_thumb'];
		$img = wp_get_attachment_url($img_id);
		
		echo '<li>';
		echo '<dl class="cf">';
		if(!empty($img)){
		echo '<dt class="fll"><a href="'.$file.'" target="_blank"><img src="'.$img.'"></a></dt>';};
		echo '<dd';
		if(!empty($img)){	echo ' class="flr';};
		echo '"><a href="'.$file.'" target="_blank">'.$file_title.'</a></dd>';
		echo '</dl>';
		echo '</li>';
	 endforeach;?>
</ul>
</div>

<?php get_footer(); ?>
