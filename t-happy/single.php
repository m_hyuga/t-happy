<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div id="content">
<div class="pcbgh2"><h2>新着情報</h2></div>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<dl class="detail_page">
<dt>
<date><?php the_time('Y.m.d'); ?></date>
<h3><?php the_title(); ?></h3>
</dt>
<dd class="cf">
<?php
/*	$repeat_group = scf::get('thumbnail_field');
	$thumb_cnt = 0;
	
	foreach ( $repeat_group as $field_name => $field_value ) :
		$val =  $field_value["thumbnail_img"];
		$image = wp_get_attachment_image_src($val, 'full');
		if (!empty($val)) {
			$thumb_cnt++;
			if ($thumb_cnt == 1) { echo '<div class="flr"><ul class="cf">';}
			echo '<li>';
			echo '<a href="'.$image[0].'" class="colorbox"><img src="'.$image[0].'"></a>';
			echo '</li>';
		}
	 endforeach;
		if ($thumb_cnt != 0) { echo '</ul></div><div class="fll">';}
*/
?>
	<div class="mceContentBody">
	<?php the_content(); ?>
	</div>
	</div>
</dd>
</dl>
<?php endwhile; endif;?>
</div>
<p class="btn_area"><a href="<?php bloginfo('url'); ?>/information/">一覧に戻る</a></p>

<?php get_footer(); ?>
